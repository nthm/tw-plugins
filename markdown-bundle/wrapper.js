/*\
title: $:/plugins/local/markdown-bundle/wrapper.js
type: application/javascript
module-type: parser

Wrapper for the markdown-it parser. Forked from anstosa/markdown-bundle.
\*/
(() => {
  'use strict'
  const req = file => require(`$:/plugins/local/markdown-bundle/${file}`)

  const MarkdownIt = req('markdown-it.min.js')
  const mdTasklist = req('markdown-it-task-lists.min.js')
  const mdEmoji = req('markdown-it-emoji.min.js')
  const hljs = req('highlight.pack.min.js')

  const TYPE_WIKI = 'text/vnd.tiddlywiki'

  let dom
  if (typeof(window) !== 'undefined') {
    dom = new DOMParser()
  }

  const md = new MarkdownIt({
    // From markdown-it documentation
    highlight: function (str, lang) {
      if (lang && hljs.getLanguage(lang)) {
        try {
          return hljs.highlight(lang, str).value
        } catch (__) {}
      }  
      return ''
    },
    html: true,
    linkify: true,
    typographer: true,
  })

  md.use(mdTasklist, {
    // Disable checkboxes so they can't be clicked. This is the default
    enabled: false,
  })

  md.use(mdEmoji, {
    // Disable all shortcuts like :) --> :smile:
    shortcuts: {},
  })

  const tiddlify = (node, forceText) => {
    if (node.nodeType === Node.TEXT_NODE) {
      let subtree
      try {
        const children = new $tw.Wiki.parsers[TYPE_WIKI](TYPE_WIKI, node.textContent, {}).tree[0].children
        if (children.length === 0) {
          subtree = null
        }
        else if (children.length === 1) {
          subtree = children[0]
          subtree.text = node.textContent
        }
        else {
          subtree = {
            type: 'element',
            tag: 'span',
            children,
          }
        }
      }
      catch(error) {
        subtree = null
      }
      if (!forceText && subtree) {
        return subtree
      }
      else {
        return {
          text: node.textContent,
          type: 'text',
        }
      }
    }
    if (node.tagName) {
      const widget = {
        attributes: {},
        tag: node.tagName.toLowerCase(),
        type: 'element',
      }
      $tw.utils.each(node.attributes, attribute => {
        widget.attributes[attribute.nodeName] = {
          type: 'string',
          value: attribute.nodeValue,
        }
      })
      widget.children = []
      node.childNodes.forEach(child => {
        const isPlainText = forceText || widget.tag === 'code' || widget.tag === 'a'
        widget.children.push(tiddlify(child, isPlainText))
      })
      return widget
    }
  }

  class MarkdownParser {
    constructor(type, text) {
      const source = md.render(text)
      const tree = dom.parseFromString(source, 'text/html')
      this.tree = tiddlify(tree.body).children
    }
  }

  exports['text/x-markdown'] = MarkdownParser
})()

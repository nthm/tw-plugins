# Markdown Bundle

This is a patched and extended fork of https://github.com/anstosa/tw5-markdown/.
Their version has a bug, but the fix is posted in a GitHub issue. I've adjusted
the plugins used by _markdown-it_. Depending on the languages you pick for
Highlight.js, this fork can be around **200kb**, which is 400kb lighter than the
original.

There's already an official Markdown plugin by the TW author, but it isn't
extendable with plugins like _markdown-it_ is.

## Features

- All the Markdown features you'd expect.
- Emojis
- Checklists via `[ ]` and `[x]`
- Syntax highlighting

## Plugins for `markdown-it`

Keep your TW light. Put a little effort into seeing what's under the hood of a
plugin. Here's links to the minified files used by this plugin. Save them into
the `files/` directory to be picked up by `require()` statements in wrapper.js.

https://github.com/revin/markdown-it-task-lists/blob/master/dist/markdown-it-task-lists.min.js
https://github.com/markdown-it/markdown-it/blob/master/dist/markdown-it.min.js
https://github.com/markdown-it/markdown-it-emoji/tree/master/dist/markdown-it-emoji.min.js

This fork uses _markdown-it-task-lists_ instead of _markdown-it-checkbox_
because tasks is 10 months old instead of 10 years, and 2kb instead of 17kb.

```
files/
├── highlight.pack.min.js
├── markdown-it-emoji.min.js
├── markdown-it.min.js
├── markdown-it-task-lists.min.js
└── solarized-light.css
```

To reduce the size of your plugin, try customizing a build of Highlight.js to
include only the languages you need: https://highlightjs.org/download/

The default includes all languages which is 514kb minified. Reducing the list to
HTML, CSS, XML, JS, HTTP, Python, JSON, Diff, Markdown, and shell session is
**16.4kb** min. It's significant.

I needed to modify _highlight.pack.min.js_ to get TW to boot. Otherwise it
complains (rightfully) `ReferenceError: hljs is not defined`. The full +500kb
version works out of the box, but smaller ones do not. However, TW has an
official Highlight.js plugin, so I checked their source:

Theirs:

```js
var hljs = require("$:/plugins/tiddlywiki/highlight/highlight.js");
/*! highlight.js v9.13.1 | BSD3 License | git.io/hljslicense */
!function(e){var n="object"==typeof window&&window||"object"==typeof self&&self;
...
```

Mine:

```js
/*! highlight.js v9.13.1 | BSD3 License | git.io/hljslicense */
!function(e){var n="object"==typeof window&&window||"object"==typeof self&&self;
...
```

Prepending the `require()` statement works great, although it sounds recursive.

# Plugins for TiddlyWiki 5

GIF of TiddlyWiki5 with some notes:

![](./pagemap-demo.gif)

There are many plugins in the official plugin library in TW. You can drag and
drop plugins between TWs too (!). Making a plugin isn't very hard and can be
useful since many plugins may be unmaintained for years or no longer work.

Decided I needed to create these plugins:

- [x] Markdown bundle
- [ ] Pagemap (see GIF above)
- [ ] Minimize/Save StoryList
- [ ] MathJAX for LaTeX equations

Each has a subfolder in this repo. Details about the plugin are there. Packaged
versions are under `export/`. Import the file to load the plugin.

## Development

[ThirdFlow][1] helps develop plugins inside of TW. Once you understand the
format of a plugin it _might_ be better to just do it outside the browser.

Plugins are packed/bundled. After development the source files are concatenated
into one `.tld` file.

## Packaging

Had a hard time learning how to package source code into a single file plugin.

There's an [official page][2] that discusses it. Need to add your plugin source
files to the `plugins/` folder in TW's source. Then generate a new TW with:

```sh
node <tw-github-repo>/tiddlywiki.js editions/tw5.com --build index
```

Open the produced HTML file, find the plugin, and export it as a ".tld".

[1]: https://github.com/TheDiveO/ThirdFlow
[2]: https://tiddlywiki.com/dev/static/Developing%2520plugins%2520using%2520Node.js%2520and%2520GitHub.html

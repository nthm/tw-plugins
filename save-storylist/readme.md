# TODO

Would like to make a OneTab-like plugin that moves all your notes to a workspace
to be picked up later. Sometimes you're in the middle of writing multiple notes
about one topic and need to switch to another idea. Workspaces would be useful
for that. Similar to Git worktrees and the proposed _kin_ feature of TW.
